#!/bin/bash

# Import config vars
source ./share/config.sh

# Проверка подключения к БД и существования таблицы для импорта
psql -h $HOST -p $PORT -U $DB_USER -d $DB_NAME -t -q -c "EXPLAIN SELECT geom FROM $DB_IMPORT_TABLE;" &> /dev/null
case "$?" in

 1 )
 echo -e "\e[31mError in psql or import table with geom column not found...Exit with code 1\e[0m"
 exit 1
 ;;

 2 )
 echo -e "\e[31mConnection failed...Exit with code 2\e[0m"
 exit 2
 ;;
 3 )
 echo -e "\e[31mImport table with geom column not found...Exit with code 3\e[0m"
 exit 3
 ;;

esac

sql_file=$(mktemp)

for f in $PATH_TO_JSON
do
  fileName=$(basename $f)
  echo -e "\e[32mProcessing $fileName file...\e[0m" 
  wkt=$(sed -r -f sed/json2wkt.sed $f)
  echo -e \
  "INSERT INTO $DB_IMPORT_TABLE(geom) \
  VALUES (ST_SetSrid(ST_GeometryFromText('$wkt'),$SRID)) \
  ON CONFLICT (uuid) DO NOTHING;" \
  >> $sql_file
done

psql -h $HOST -p $PORT -U $DB_USER -d $DB_NAME -f $sql_file
rm $sql_file
exit 0
